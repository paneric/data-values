<?php

declare(strict_types=1);

namespace Paneric\DataValues;

class ZipPlValue extends ZipValue
{
    protected $notFormatedValue;

    public function set(string $value): void
    {
        if (!preg_match( '/^(\d{2})-(\d{3})$/', $value,  $matches)) {
            return;
        }

        $this->matches = $matches;

        $this->notFormatedValue = $value;
    }
}
