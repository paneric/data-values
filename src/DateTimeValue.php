<?php

declare(strict_types=1);

namespace Paneric\DataValues;

use DateTimeImmutable;
use DateTimeZone;
use Exception;

class DateTimeValue implements DataValueInterface
{
    protected $dateTimeImmutable;
    protected $config = [
        'date_time_zone' => 'Europe/Warsaw',
        'pattern' => 'Y-m-d H:i:s'
    ];

    protected $matches;

    public function __construct(string $dateTime = 'now', string $pattern = null)
    {
        $this->set($dateTime, $pattern);
    }

    public function get(): ?DateTimeImmutable
    {
        return $this->dateTimeImmutable;
    }

    public function set(string $value, string $pattern = null): void
    {
        if (!preg_match(
            '/^(\d{4})-([0-1]\d)-([0-3]\d)\s([0-1]\d|[2][0-3]):([0-5]\d):([0-5]\d)$/',
            $value,
            $matches)
        ) {
            return;
        }

        $this->matches = $matches;

        if ($value === 'now') {
            $this->createNow();
            return;
        }

        $this->create($value, $pattern);
    }

    public function format(string $pattern = null): ?string
    {
        if ($this->dateTimeImmutable === null) {
            return null;
        }

        return $this->dateTimeImmutable->format($pattern ?? $this->config['pattern']);
    }

    protected function createNow(): void
    {
        try {
            $this->dateTimeImmutable = new DateTimeImmutable(
                'now',
                new DateTimeZone($this->config['date_time_zone'])
            );
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    protected function create(string $dateTime, string $pattern = null): void
    {
        try {
            $this->dateTimeImmutable = DateTimeImmutable::createFromFormat(
                $pattern ?? $this->config['pattern'],
                $dateTime,
            );
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
