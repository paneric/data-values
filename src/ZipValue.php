<?php

declare(strict_types=1);

namespace Paneric\DataValues;

class ZipValue implements DataValueInterface
{
    protected $notFormatedValue;

    protected $matches;

    public function __construct(string $value)
    {
        $this->set($value);
    }

    public function get(): ?string
    {
        return $this->notFormatedValue;
    }

    public function set(string $value): void
    {
        $value = preg_replace( '/\s+/', '', $value );

        if (!preg_match('/^([A-Z\d]{4-10})$/', $value, $matches)) {
            return;
        }

        $this->matches = $matches;

        $this->notFormatedValue = $value;
    }

    public function format(): ?string
    {
        if ($this->matches === null) {
            return null;
        }

        return $this->matches[0];
    }
}

