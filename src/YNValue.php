<?php

declare(strict_types=1);

namespace Paneric\DataValues;

class YNValue implements DataValueInterface
{
    protected $notFormatedValue;

    protected $matches = ['no', 'yes'];

    public function __construct(string $value)
    {
        $this->set($value);
    }

    public function get(): ?string
    {
        return $this->notFormatedValue;
    }

    public function set(string $value): void
    {
        $value = preg_replace( '/\s+/', '', $value );

        if (!array_key_exists((int) $value, $this->matches)) {
            return;
        }

        $this->notFormatedValue = $value;
    }

    public function format(): ?string
    {
        if ($this->notFormatedValue === null) {
            return null;
        }

        return $this->matches[(int) $this->notFormatedValue];
    }
}
