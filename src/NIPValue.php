<?php
declare(strict_types=1);

namespace Paneric\DataValues;

class NIPValue extends TVAValue
{
    protected $notFormatedValue;

    public function set(string $value): void
    {
        if (!preg_match( '/^PL(\d{3})(\d{3})(\d{2})(\d{2})$/', $value,  $matches)) {
            return;
        }

        $this->matches = $matches;

        $this->notFormatedValue = $value;
    }
}
