<?php

declare(strict_types=1);

namespace Paneric\DataValues;

interface DataValueInterface
{
    public function get();

    public function set(string $value): void;

    public function format(): ?string;
}
