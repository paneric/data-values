<?php

declare(strict_types=1);

namespace Paneric\DataValues;

class MoneyValue extends NumberValue
{
    private $suffix;

    public function __construct($numberValue, string $currency = '€', array $config = null)
    {
        parent::__construct($numberValue, $config);

        $this->suffix = $currency;
    }

    public function format(): ?string
    {
        if ($this->notFormatedValue === null) {
            return null;
        }

        return sprintf(
            '%s %s',
            $this->notFormatedValue,
            $this->suffix
        );
    }
}
