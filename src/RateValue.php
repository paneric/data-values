<?php

declare(strict_types=1);

namespace Paneric\DataValues;

class RateValue extends NumberValue
{
    private $suffix;

    public function __construct($value, array $config = null, string $suffix = '%')
    {
        parent::__construct($value, $config);

        $this->suffix = $suffix;
    }

    public function format(): ?string
    {
        if ($this->notFormatedValue === null) {
            return null;
        }

        return sprintf(
            '%s %s',
            $this->notFormatedValue,
            $this->suffix
        );
    }
}
