<?php

declare(strict_types=1);

namespace Paneric\DataValues;

class NumberValue implements DataValueInterface
{
    protected $notFormatedValue;

    protected $config = [
        'decimals' => 2,
        'decimals_point' => ',',
        'thousands_separator' => ' ',
    ];

    public function __construct($value, array $config = null)
    {
        if ($config !== null) {
            $this->config = $config;
        }

        $this->set($value);
    }

    public function get(): ?string
    {
        return $this->notFormatedValue;
    }

    public function set($value): void
    {
        $value = preg_replace( '/\s+/', '', $value );

        if (!filter_var($value, FILTER_VALIDATE_FLOAT)) {
            return;
        }

        $this->notFormatedValue = number_format(
            $this->notFormatedValue,
            $this->config['decimals'],
            $this->config['decimals_point'],
            $this->config['thousands_separator']
        );
    }

    public function format(): ?string
    {
        return $this->notFormatedValue;
    }
}
